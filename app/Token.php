<?php

namespace App;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Token
 *
 * @package App
 */
class Token extends Model
{
    protected $fillable = ['token', 'refresh', 'expires_at'];

    protected $hidden = ['token', 'refresh'];

    /**
     * The Guzzle client
     *
     * @var Client
     */
    private static $_client;

    /**
     * Gets a valid token for Reddit
     *
     * @return string|null
     */
    public static function getToken()
    {
        self::useClient();
        $tkn = Token::orderBy('id', 'desc')->first();
        if ($tkn == null) {
            return null;
        }
        $expires = Carbon::parse($tkn->expires_at);
        if ($expires->lessThan(Carbon::now())) {
            $resp = self::$_client->post(
                '/api/v1/access_token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $tkn->refresh
                ],
                'auth' => [
                    env('REDDIT_CID'),
                    env('REDDIT_SECRET')
                ],
                'headers' => [
                    'user-agent' => 'laravel:dailypost:v1.0 (by /u/idevjoe)'
                ]
                ]
            );
            $js = json_decode($resp->getBody());
            Token::create(
                ['token' => $js->access_token, 'refresh' => $tkn->refresh,
                'expires_at' => Carbon::now()->addSeconds($js->expires_in)]
            );
            return $js->access_token;
        }
        return $tkn->token;
    }

    /**
     * Sets up the client object
     *
     * @return void
     */
    public static function useClient()
    {
        if (self::$_client == null) {
            self::$_client = new Client(
                ['base_uri' => 'https://www.reddit.com', 'defaults' => [
                'headers' => [
                'user-agent' => 'laravel:dailypost:v1.0 (by /u/idevjoe)'
                ]
                ]]
            );
        }
    }

    /**
     * Gets a token from an authorization code
     *
     * @param string $code The authorization code
     *
     * @return mixed
     */
    public static function tokenFromCode($code)
    {
        self::useClient();
        $resp = self::$_client->post(
            '/api/v1/access_token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => route('reddit.oauthprocess')
            ],
            'auth' => [
                env('REDDIT_CID'),
                env('REDDIT_SECRET')
            ],
            'headers' => [
                'user-agent' => 'laravel:dailypost:v1.0 (by /u/idevjoe)'
            ]
            ]
        );
        $js = json_decode($resp->getBody());
        Token::create(
            ['token' => $js->access_token, 'refresh' => $js->refresh_token,
            'expires_at' => Carbon::now()->addSeconds($js->expires_in)]
        );
        return $js->access_token;
    }
}
