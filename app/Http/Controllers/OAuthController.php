<?php
namespace App\Http\Controllers;


use App\Token;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OAuthController
 *
 * @package App\Http\Controllers
 */
class OAuthController
{

    private $_client;

    /**
     * OAuthController constructor.
     */
    public function __construct()
    {
        $this->_client = new Client(
            [
            'base_uri' => 'https://www.reddit.com', 'defaults' => [
            'headers' => [
                'user-agent' => 'laravel:dailypost:v1.0 (by /u/idevjoe)'
            ]
            ]]
        );
    }

    /**
     * Redirects to Reddit OAuth. Restricted.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function OAuthLink()
    {
        if (env('APP_ENV') !== 'local') {
            throw new NotFoundHttpException();
        }
        return redirect(
            'https://www.reddit.com/api/v1/authorize?client_id='
            . urlencode(env('REDDIT_CID'))
            . '&response_type=code&redirect_uri='
            . urlencode(route('reddit.oauthprocess'))
            . '&duration=permanent&scope=read&state=whoop'
        );
    }

    /**
     * Processes an oauth request
     *
     * @param Request $request The request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processOAuth(Request $request)
    {
        if (env('APP_ENV') !== 'local') {
            throw new NotFoundHttpException();
        }
        Token::tokenFromCode($request->get('code'));
        return redirect()->route('dailypost');
    }

}