<?php

namespace App\Http\Controllers\API;

use App\Token;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

/**
 * Class PostController
 *
 * @package App\Http\Controllers\API
 */
class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('pk');
    }

    /**
     * Determines if the given string starts with the query
     *
     * @param string $string The string to test
     * @param string $query  The query
     *
     * @return bool
     */
    private static function _startsWith($string, $query)
    {
        return substr($string, 0, strlen($query)) === $query;
    }

    /**
     * Encodes a snapchat sticker with a caption
     *
     * @param string $url The URL
     *
     * @return array|\Intervention\Image\Image
     */
    public static function encodeSticker($url)
    {
        $inf = self::pullPostInfo($url);
        if (isset($inf['error'])) {
            return $inf;
        }
        $img = Image::make($inf['content']);
        $text_scale = 150;
        $inc = $text_scale + 100;
        $newH = $img->height()+$inc;
        $img->resizeCanvas(
            $img->width(), intval($newH),
            'bottom-left', false, "#ffffff"
        );
        $img->text(
            $inf['subtitle'], 20, 150, function ($a) use ($text_scale) {
                $a->file(resource_path('ttf/Roboto-Thin.ttf'));
                $a->size($text_scale);
            }
        );

        return $img->encode('data-url');
    }

    /**
     * Finds post information based on the URL
     *
     * @param string $url The post URL
     *
     * @return array
     */
    public static function pullPostInfo($url)
    {
        if (!self::_startsWith($url, 'https://www.reddit.com/')) {
            return ['error' => 'Invalid'];
        }
        $url = explode('?', $url)[0];
        $modified = 'https://oauth.reddit.com/'.substr($url, 23).'.json';
        $client = new Client();
        $data = $client->get(
            $modified, ['headers' => [
            'Authorization' => 'Bearer ' . Token::getToken(),
            'User-Agent' => 'laravel:dailypost:v1.0 (by /u/idevjoe)'
            ]]
        );
        $js = json_decode($data->getBody());
        if (!isset($js[0])) {
            return ['error' => 'Invalid'];
        }
        $listing = $js[0]->data->children[0]->data;
        //return $listing;
        if ($listing->is_video) {
            return ['error' => 'Videos cannot be processed'];
        }
        return ['subtitle' => html_entity_decode($listing->title),
            'subreddit' => '/r/' . $listing->subreddit,
            'content' => $listing->url, 'author' => '/u/' . $listing->author,
            'score' => $listing->score];
    }

    /**
     * Calls PullPostInfo
     *
     * @param Request $request The request
     *
     * @return array
     */
    public function postInfo(Request $request)
    {
        return ['post' => self::pullPostInfo($request->get('url'))];
    }

    /**
     * Calls EncodeSticker
     *
     * @param Request $request The request
     *
     * @return array
     */
    public function encodeS(Request $request)
    {
        return ['b64' => self::encodeSticker($request->get('url'))];
    }
}
