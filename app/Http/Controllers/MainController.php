<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * The main controller responsible for handling
 * misc requests such as the homepage
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * Returns the main view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('home');
    }

    /**
     * Gets a list of posts
     *
     * @deprecated
     * @return     \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getposts()
    {
        return view('dailypost');
    }

    /**
     * Shows the license app
     *
     * @return mixed
     */
    public function showLicenseApp() {
        return view('license.index');
    }

    public function genBC($code) {
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        return response($generator->getBarcode($code, $generator::TYPE_CODE_39, 2, 50), 200)->header('Content-Type', 'image/png');
    }
}
