<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Validates a post key
 *
 * @package App\Http\Middleware
 */
class PostKey
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request The request
     * @param \Closure                 $next    The next function
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('X-PostKey') != env('POSTKEY')) {
            throw new UnauthorizedHttpException('');
        }
        return $next($request);
    }
}
