# devjoe.net

This is the official code repository for devjoe.net

## Setup

- Copy .env.example to .env
- Run `php artisan key:generate` to generate a encryption key
- Serve as you normally would (public root is /public)