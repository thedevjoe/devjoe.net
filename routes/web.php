<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home')->name('home');
Route::get('/bc/{code}', 'MainController@genBC');

Route::get('/oauth', 'OAuthController@OAuthLink')->name('reddit.oauth');
Route::get('/oauth/dp', 'OAuthController@processOAuth')->name('reddit.oauthprocess');

Route::get('/license', 'MainController@showLicenseApp')->name('license');
Route::get('/license/{any}', 'MainController@showLicenseApp')->where('any', '.*');
