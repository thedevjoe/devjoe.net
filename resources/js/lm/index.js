import Vue from 'vue';
import VueRouter from 'vue-router';
import router from "./router";
import login from './pages/login';
import homepage from "./pages/homepage";
import lv from './components/license-view';
import configeditor from './pages/config-editor';
import addlicense from './pages/add-license';
import swal from 'sweetalert2';
import KJUR from 'jsrsasign';
import JSONEditor from 'jsoneditor';
import 'jsoneditor/dist/jsoneditor.css'

Vue.use(VueRouter);
Vue.component('router', router);
Vue.component('login', login);
Vue.component('license-view', lv);
Vue.component('add-license', addlicense);

window.jsoneditor = JSONEditor;

const routes = [
    {path: '/home', component: homepage},
    {path: '/home/:id/config', component: configeditor},
    {path: '/add', component: addlicense}
];

window.router = new VueRouter({routes});
window.swal = swal;

window._f = (url) => {
    return process.env.MIX_LICENSE_API_HOME + url;
};

window.KJUR = KJUR;

window.getPubKey = () => {
    if(localStorage.licenseJWSKey !== undefined) return new Promise((res, rej) => {
        res(localStorage.licenseJWSKey);
    });
    return new Promise((res, rej) => {
        fetch(_f("/jwt/pubkey")).then(e => e.text()).then(pubkey => {
            localStorage.licenseJWSKey = pubkey;
            res(pubkey);
        });
    });
};

window.app = new Vue({
    el: "#app",
    router: window.router,
    data: () => {
        return {user: null}
    }
});
