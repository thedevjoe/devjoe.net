@extends('layouts.app')

@section('content')
    <div class="container mt-5 mb-5">
        <h1>Daily Post <small class="text-muted">by Joe Longendyke</small></h1>
        <div class="row">
            <div class="col-md-6">
                <h4>Today</h4>
            </div>
            <div class="col-md-6">
                <h4>Yesterday</h4>
            </div>
        </div>
    </div>
@endsection