@extends('layouts.app', ['exclude_nav' => true])

@section('header')
    <style>
        body, html {
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .links a {
            color: #000000;
            text-transform: uppercase;
            margin-right: 30px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    {{--<div class="bg-darker-primary w-100 text-light">
        <div class="container pt-5 pb-5">
            <div class="center">
                <h1 class="display-4">I'm Joe Longendyke,</h1>
                <h1>A full stack software developer from Minnesota.</h1>
            </div>
        </div>
        <div class="container pb-5 informationPopup">
            <div class="d-flex justify-content-center">
                <a href="https://git.devjoe.net/joe" class="btn-outline-light btn ml-1">Gitlab</a>
                <a href="https://github.com/idevjoe" class="btn-outline-light btn ml-1">Github</a>
                <a href="mailto:joe@devjoe.net" class="btn-outline-light btn ml-1">Email</a>
            </div>
        </div>
    </div>
    <div class="text-dark bg-light">
        <div class="container center pt-5 pb-5">
            <h2>Basic Information</h2>
            <span class="bold-display">
                I'm a full stack developer born and raised in Minnesota. Due to the cold and snow, I've spent most
                 of my life indoors writing code. Because of this, I've written more code than I can remember
                 and can tackle as many challenges as you can throw at me.
            </span>
        </div>
    </div>
    <div class="text-light bg-darker-primary pt-5 pb-5">
        <div class="container">
            <div class="center mb-4">
                <h2>Past Projects</h2>
            </div>
            <div class="row">
                <div class="col-md-4 border-right">
                    <h4>Robotics Team Website</h4>
                    <p>
                        While I was in High-School, I participated in the FIRST Robotics program for two years.
                         During these two years, myself and <a href="https://quinnzipse.dev" class="text-white-50" target="_blank">Quinn Zipse</a>
                         created a fully functional team website.
                    </p>
                </div>
                <div class="col-md-4 border-right">
                    <h4>FlightDash</h4>
                    <p>
                        FlightDash is a system that simulates a real airline flight crew portal. It simulates features
                         such as dispatching and can sort and display real up-to-date FAA charts.
                    </p>
                </div>
                <div class="col-md-4">
                    <h4>IDJ License</h4>
                    <p>
                        IDJ License is a proprietary licensing system for many of my public and private projects. It is
                         meant to collect anonymous analytics as well as provide authentication to use private projects.
                    </p>
                </div>
            </div>
            <div class="center mt-4">
                <em>Projects above are not open source and demos must be requested by email.</em>
            </div>
        </div>
    </div>
    <div class="text-dark bg-light">
        <div class="container pt-5 pb-5">
            <div class="center">
                <h2>Contact</h2>
                <span>
                    To find out more information about me and services I can provide, please email <a href="mailto:joe@devjoe.net">joe@devjoe.net</a>.
                     I will get back to you within 1-5 business days.
                </span>
            </div>
        </div>
    </div>
    <div class="pt-5 pb-5 border-top">
        <div class="container">
            &copy; 2020 devjoe.net
        </div>
    </div>--}}
    <div class="container pt-5 pb-5 h-100">
        <div class="d-flex h-100 align-items-center">
            <div>
                <h1 class="display-1 text-black-50">Joe Longendyke</h1>
                <p>
                    I shoot cool guns, see cool things, and program on the side.
                </p>
                <div class="links">
                    <a href="https://gitlab.com/thedevjoe">Stalk me on Gitlab</a>
                    <a href="mailto:joe@devjoe.net">Email me</a>
                </div>
                <hr />
            </div>
        </div>

    </div>
@endsection

@section('footer')
    <script src="{{ asset('/js/homeEasterEggs.js') }}"></script>
@endsection
