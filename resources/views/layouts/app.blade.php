<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />
    <title>Joe Longendyke</title>
    @yield('header')
</head>
<body>
    @if(!isset($exclude_nav))
        @include('components.nav')
    @endif
    @yield('content')
    <script src="{{ asset('/js/app.js') }}"></script>
    @yield('footer')
</body>
</html>
